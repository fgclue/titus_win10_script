<!-- # ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀Ultimate Windows Toolbox !-->
<p align=center>
    <img src="uwtw.png">
    <img src="https://img.shields.io/badge/Powershell-blue?logo=Powershell&logoColor=white"> <img src="https://img.shields.io/badge/Windows_10-blue?logo=Windows&logoColor=white"> <img src="https://img.shields.io/badge/GitHub-black?logo=Github&logoColor=white"> <img src="https://img.shields.io/badge/Terminal-black?logo=Windows+Terminal&logoColor=white"> <img src="https://img.shields.io/badge/Tested-darkblue?logo=VirtualBox&logoColor=white">
</p>
This script is the culmination of many scripts and gists from github with features of my own. I am building this script to be a swiss army knife of Windows tools to help setup and optimize machines.

## What is in this script?
- One command to run
- Full GUI implementation
- Winget install
- Install popular programs with one click
- O&O Shutup 10 CFG and Run
- Dark/Light mode
- Semi-configurable

## How to Run
# Graphical Install
Paste this command into Powershell (admin):
```
iex ((New-Object System.Net.WebClient).DownloadString('https://t.ly/OUwh'))
```

Or, Shorter:
```
iwr -useb https://t.ly/OUwh | iex
```

# CLI Install
Paste this command into Powershell (admin):
```
iex ((New-Object System.Net.WebClient).DownloadString('https://t.ly/YgR_')) 
```
Or, shorter:
```
iwr -useb https://t.ly/YgR_ | iex
```

For complete details check out https://christitus.com/debloat-windows-10-2020/


For changelogs, Go to changelog.md.
